/**
 
 use `pod install` to download all dependencies
 
 The Architecture: (or why not MVC)
 MVVM-C (Model-View-ViewModel-Coordinator)
 Inspired by Steve Scott talk at UIKonf 2016 "MVVM-C in Practice"
 
**/

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    func application(_ application: UIApplication,
                            didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        window = UIWindow()
        appCoordinator = AppCoordinator(window: window!)
        appCoordinator.render(withParentViewModel: nil)
        window?.makeKeyAndVisible()
        
        return true
    }
}
