import UIKit

class BaseViewController: UIViewController, ViewControllerProtocol {
    
    var navigationBarTitle: String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        configureUI()
        configureTemplate()
    }
    
    func configureUI() {
        
        fatalError( Constant.ErrorMessage.MethodOverride.rawValue )
    }
  
    func configureTemplate() {
        
        fatalError( Constant.ErrorMessage.MethodOverride.rawValue )
    }
    
    func setupNavigationTitle(text: String) {
        
        fatalError( Constant.ErrorMessage.MethodOverride.rawValue )
    }
    
    func updateView() {
        
        fatalError( Constant.ErrorMessage.MethodOverride.rawValue )
    }
}
