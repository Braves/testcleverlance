import UIKit

class PostDetailViewController: BaseViewController {
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    
    var viewModel: PostDetailViewModel? {
        
        willSet {
            
            viewModel?.viewDelegate = nil
        }
        didSet {
            
            viewModel?.viewDelegate = self
        }
    }
 
    override func configureTemplate() {
        
        view.backgroundColor = UIColor.orange
    }
    
    override func configureUI() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.setup()
        
        navigationItem.title = navigationBarTitle
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        view.addGestureRecognizer(recognizer)
    }
    
    override func updateView() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        
        postTitle.text = viewModel.title() ?? "No Title"
        
        if let imageData = viewModel.image() {
            
            postImage.image = UIImage(data: imageData)
        } else {
            
            postImage.image = UIImage(named: "placeholder")
        }
        
    }
    
    func dismissVC() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.finish()
    }
    
}

extension PostDetailViewController: PostDetailViewModelViewDelegate {
    
    func updateContent(viewModel: ViewModelProtocol) {
        updateView()
    }
}
