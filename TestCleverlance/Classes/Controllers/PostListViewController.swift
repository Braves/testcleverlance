import UIKit

class PostListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: PostListViewModel? {
        
        willSet {
            
            viewModel?.viewDelegate = nil
        }
        didSet {
            
            viewModel?.viewDelegate = self
        }
    }
    
    override func configureTemplate() {
        
        view.backgroundColor = UIColor.clear
        tableView.backgroundColor = UIColor.clear
    }
    
    override func configureUI() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.setup()
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constant.Cell.PostListCell, bundle: nil), forCellReuseIdentifier: Constant.Cell.PostListCell)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
            tableView.refreshControl?.backgroundColor = UIColor.white
            tableView.refreshControl?.tintColor = UIColor.black
            tableView.refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)
        }
        
        navigationItem.title = navigationBarTitle
    }
    
    override func updateView() {
        
        tableView.reloadData()
    }
    
    func reload() {
        
        guard let viewModel = viewModel else { fatalError( Constant.ErrorMessage.ObjectNotFound.rawValue ) }
        viewModel.downloadListOfPosts()
    }

}

extension PostListViewController: PostListViewModelViewDelegate {
    
    func updateContent(viewModel: ViewModelProtocol) {
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl?.endRefreshing()
        }
        
        updateView()
    }
}

extension PostListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 85.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let viewModel = viewModel else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        viewModel.selectEntry(at: indexPath.row)
    }
}

extension PostListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Cell.PostListCell, for: indexPath) as? PostListTableViewCell else { fatalError(Constant.ErrorMessage.CellRegistration.rawValue) }
        
        guard let viewModel = viewModel else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        cell.titleLabel.text = viewModel.title(at: indexPath.row) ?? ""
        
        if let imageData = viewModel.image(at: indexPath.row) {
            
            cell.imgView.image = UIImage(data: imageData)
        } else {
            
            cell.imgView.image = UIImage(named: "placeholder")
        }
        
        cell.selectionStyle = .none
        cell.imgView.contentMode = .scaleToFill
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        return viewModel.count()
    }
    
}
