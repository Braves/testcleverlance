import Foundation

protocol ViewControllerProtocol {
    
    var navigationBarTitle: String { get set }
    func configureTemplate()
    func configureUI()
    func updateView()
}
