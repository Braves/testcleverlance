import UIKit

class AppCoordinator: CoordinatorProtocol {
    
    var isInitialView = false

    fileprivate let kPostListCoordinator = "PostList"
    fileprivate let kPostDetailCoordinator = "PostDetail"
    
    var window: UIWindow
    var coordinators = [String:CoordinatorProtocol]()
    
    init(window: UIWindow) {
        
        self.window = window
    }

    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        showPostList(viewModel)
    }
}

extension AppCoordinator: PostListCoordinatorDelegate {
    
    func showPostList(_ viewModel: ViewModelProtocol?) {
        
        let postListCoordinator = PostListCoordinator(window: window)
        coordinators[kPostListCoordinator] = postListCoordinator
        postListCoordinator.delegate = self
        postListCoordinator.isInitialView = true
        postListCoordinator.render(withParentViewModel: nil)
    }
}
