import UIKit

protocol PostDetailCoordinatorDelegate: class {

    func postDetailCoordinatorDidFinish(postDetailCoordinator: PostDetailCoordinator)
}

class PostDetailCoordinator: CoordinatorProtocol {

    var isInitialView = false
    weak var delegate: PostDetailCoordinatorDelegate?
    var window: UIWindow
    var postDetailViewController: PostDetailViewController?
    
    init(window: UIWindow) {
        
        self.window = window
    }
    
    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        let storyboard = UIStoryboard(name: Constant.Config.StoryboardName, bundle: nil)
        guard let postDetailViewController = storyboard.instantiateViewController(withIdentifier: Constant.Config.PostDetailViewControllerIdentifier) as? PostDetailViewController else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        let viewModel =  PostDetailViewModel(withParentViewModel: viewModel)
        viewModel.coordinatorDelegate = self
        postDetailViewController.viewModel = viewModel
        postDetailViewController.navigationBarTitle = "Detail"
        self.postDetailViewController = postDetailViewController
        
        if isInitialView {
            
            let navigationController = UINavigationController(rootViewController: postDetailViewController)
            window.rootViewController = navigationController
        } else {
            
            guard let navigationController = window.rootViewController as? UINavigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
            navigationController.present(postDetailViewController, animated: true)
        }        
    }

}

extension PostDetailCoordinator: PostDetailViewModelDelegate {
    
    func dismiss(postDetailCoordinator: PostDetailViewModelDelegate) {
        
        if let postDetailViewController = postDetailViewController {
            
            postDetailViewController.dismiss(animated: true, completion: nil)
        }
    }
}
