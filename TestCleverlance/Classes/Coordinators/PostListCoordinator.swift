import UIKit

protocol PostListCoordinatorDelegate: class {}

class PostListCoordinator: CoordinatorProtocol {
    
    var isInitialView = false
    weak var delegate: PostListCoordinatorDelegate?
    var postDetailCoordinator: PostDetailCoordinator?
    
    var window: UIWindow
    var postListViewController: PostListViewController?

    init(window: UIWindow) {
        self.window = window
    }

    func render(withParentViewModel viewModel: ViewModelProtocol?) {
        
        let storyboard = UIStoryboard(name: Constant.Config.StoryboardName, bundle: nil)
        guard let postListViewController = storyboard.instantiateViewController(withIdentifier: Constant.Config.PostListViewControllerIdentifier) as? PostListViewController else { fatalError(Constant.ErrorMessage.ObjectNotFound.rawValue) }
        
        let viewModel =  PostListViewModel(withParentViewModel: viewModel)
        viewModel.coordinatorDelegate = self
        postListViewController.viewModel = viewModel
        postListViewController.navigationBarTitle = Constant.Screen.PostListTitle
        self.postListViewController = postListViewController
        
        if isInitialView {
            
            let navigationController = UINavigationController(rootViewController: postListViewController)
            window.rootViewController = navigationController
        } else {
            
            guard let root = window.rootViewController,
                     let navigationController = root.navigationController else { fatalError(Constant.ErrorMessage.Unknown.rawValue) }
            navigationController.pushViewController(postListViewController, animated: true)
        }
    }
}

extension PostListCoordinator: PostListViewModelDelegate {
    
    func selectPostAction(_ viewModel: ViewModelProtocol) {
        
        postDetailCoordinator = PostDetailCoordinator(window: window)
        postDetailCoordinator?.render(withParentViewModel: viewModel)
    }
}


