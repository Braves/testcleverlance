import Foundation

protocol CoordinatorProtocol: class {
    
    func render(withParentViewModel viewModel: ViewModelProtocol?)
    var isInitialView: Bool { get set }
}
