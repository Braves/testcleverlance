import Foundation

class Post: ModelProtocol {
    
    fileprivate var identifier: String?
    fileprivate var owner: String?
    fileprivate var secret: String?
    fileprivate var server: String?
    fileprivate var farm: Int?
    fileprivate var title: String?
    fileprivate var isPublic: Int?
    fileprivate var isFriend: Int?
    fileprivate var isFamily: Int?
    fileprivate var imageURL: String?
    fileprivate var picture: Data?
    
    init(_ identifier: String?, owner: String?, secret:String?, server: String?, farm: Int?, title: String?, isPublic: Int?, isFriend: Int?, isFamily: Int?) {
        
        self.identifier = identifier
        self.owner = owner
        self.secret = secret
        self.server = server
        self.farm = farm
        self.title = title
        self.isPublic = isPublic
        self.isFriend = isFriend
        self.isFamily = isFamily
    }
        
    lazy var dao: PostDao = {
        
        return PostDao(self)
    }()
}

class PostDao: BaseDaoProtocol {

    var model: ModelProtocol
    
    init(_ model: Post) {
        
        self.model = model
    }
    
    func identifier() -> String? {
        
        if let model = model as? Post {
            
            return model.identifier
        }
        
        return .none
    }
    
    func owner() -> String? {
        
        if let model = model as? Post {
            
            return model.owner
        }
        
        return .none
    }
  
    func secret() -> String? {
        
        if let model = model as? Post {
            
            return model.secret
        }
        
        return .none
    }
    
    func server() -> String? {
        
        if let model = model as? Post {
            
            return model.server
        }
        
        return .none
    }
    
    func farm() -> Int? {
        
        if let model = model as? Post {
            
            return model.farm
        }
        
        return .none
    }
    
    func title() -> String? {
        
        if let model = model as? Post {
            
            return model.title
        }
        
        return .none
    }
    
    func isPublic() -> Int? {
        
        if let model = model as? Post {
            
            return model.isPublic
        }
        
        return .none
    }
    
    func isFriend() -> Int? {
        
        if let model = model as? Post {
            
            return model.isFriend
        }
        
        return .none
    }
    
    func isFamily() -> Int? {
        
        if let model = model as? Post {
            
            return model.isFamily
        }
        
        return .none
    }
    
    func picture() -> Data? {
        
        if let model = model as? Post {
            
            return model.picture
        }
        
        return .none
    }
    
    func picture(with data: Data?) {
        
        if let model = model as? Post {
            
            model.picture = data
        }
    }

    func imageURL() -> String? {
        
        if let model = model as? Post,
            let farm = model.farm,
            let server = model.server,
            let id = model.identifier,
            let secret = model.secret {
            
            return "https://farm\(String(farm)).staticflickr.com/\(server)/\(id)_\(secret).jpg"
        }
        
        return .none
    }
}
