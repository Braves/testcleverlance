struct Constant {
    
    private static let description = "Constant"
    
    enum ErrorMessage: String {
        case MethodOverride = "Should be overriden in child!"
        case ObjectNotFound = "Object was not found!"
        case CellRegistration = "Cell was not registered!"
        case Unknown = "Something went wrong!"
    }
    
    struct Screen {
        private static let description = "Screen"
        
        static let PostListTitle = "Posts"
    }
    
    struct Cell {
        private static let description = "Cell"
        
        static let PostListCell = "PostListTableViewCell"
        static let PostDetailCell = "PostDetailTableViewCell"
    }
    
    struct Config {
        private static let description = "Config"
        
        static let StoryboardName = "TestCleverlance"
        static let PostListViewControllerIdentifier = "PostListViewControllerIdentifier"
        static let PostDetailViewControllerIdentifier = "PostDetailViewControllerIdentifier"
    }
    
    struct API {
        private static let description = "API"
        
        static let FlickrKey = "e43967fa55cb342397646ba805b4c64c"
        
        static let Base = "https://api.flickr.com/services/rest"
        static let ApiKey = "&api_key=\(FlickrKey)"
        static let Tags = "&tags=Apple"
        static let Page = "&page="
        static let FormatJson = "&format=json"
        static let PerPage = "&per_page=10"
        static let NoJsonCallback = "&nojsoncallback=1"
        static let ContentType = "application/json"
        
        struct GET {
            private static let description = "GET"
            
            static let PostList = [Constant.API.Base,"?&method=flickr.photos.search"].joined(separator: "/").appending("\(Constant.API.ApiKey)\(FormatJson)\(NoJsonCallback)\(Constant.API.Tags)\(PerPage)\(Constant.API.Page)")
        }

    }
}
