import Foundation
import SwiftyJSON

protocol MappingServiceProtocol: class {    
    
    func deserializeListOfPosts(_ data: Data) -> [ModelProtocol]?
    func deserializePostImage(_ data: Data) -> Data?
}

class MappingService: MappingServiceProtocol {
  
    func deserializeListOfPosts(_ data: Data) -> [ModelProtocol]? {
        
        let json = JSON(data: data)
        let photos = json["photos"]
        guard let responseDict = photos.dictionary else { return .none }
        guard let photoArray = responseDict["photo"]?.array else { return .none }
        
        var models = [ModelProtocol]()
        for item in photoArray {
    
            let model = Post(item["id"].string,
                                          owner: item["owner"].string,
                                          secret: item["secret"].string,
                                          server: item["server"].string,
                                          farm: item["farm"].int,
                                          title: item["title"].string,
                                          isPublic: item["ispublic"].int,
                                          isFriend: item["isfriend"].int,
                                          isFamily: item["isfamily"].int)
            
            models.append(model)
        }
        
        return models
    }
    
    func deserializePostImage(_ data: Data) -> Data? {
        
        return data
    }
   
}
