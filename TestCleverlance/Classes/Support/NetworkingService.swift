import Foundation
import Alamofire
import AlamofireImage

protocol NetworkingProtocol: class {
    
    func getList(forPage page:String?, withBlock block: @escaping (Data?)->())
    func getImages(for url: String?, withBlock block: @escaping (Data?)->())
}

class NetworkingService: NetworkingProtocol {
    
    func getList(forPage page:String?, withBlock block: @escaping (Data?)->()) {
        
        if let page = page {
            let urlString = Constant.API.GET.PostList.appending(page)
            
            if let url = URL(string: urlString) {
            
                Alamofire.request(URLRequest(url: url))
                    .responseJSON { response in
                        
                        if let error = response.result.error {
                            
                            print(error.localizedDescription)
                            return
                        }
                        
                        block(response.data)
                }
            }
        }
    }
    
    func getImages(for url: String?, withBlock block: @escaping (Data?)->()) {
        
        if let url = url {
            Alamofire.request(url).responseImage { response in
                
                if let _ = response.result.value {
                    
                    block(response.data)
                } else {
                    
                    print("Image Donwload error")
                    return
                }
            }        
        }
    }
    
}
