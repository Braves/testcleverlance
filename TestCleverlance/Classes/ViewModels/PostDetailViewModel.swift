import Foundation

protocol PostDetailViewModelViewDelegate: class {
    
    func updateContent(viewModel: ViewModelProtocol)
}

protocol PostDetailViewModelDelegate: class {
    
    func dismiss(postDetailCoordinator: PostDetailViewModelDelegate)
}

class PostDetailViewModel: BaseViewModel {
    
    weak var viewDelegate: PostDetailViewModelViewDelegate?
    weak var coordinatorDelegate: PostDetailViewModelDelegate?
        
    var model: ModelProtocol? {
        
        didSet {
            
            if let viewDelegate = self.viewDelegate {
                
                viewDelegate.updateContent(viewModel: self)
            }
        }
    }
    
    override func setup() {
        
        if let parentViewModel = parentViewModel as? PostListViewModel,
            let selected = parentViewModel.selectedModel,
            let model = selected as? Post {
            
            self.model = model
        }
    }
    
    func title() -> String? {
    
        if let model = model as? Post {
            
            return model.dao.title()
        }
        
        return .none
    }
    
    func image() -> Data? {
        
        if let model = model as? Post {
            
            return model.dao.picture()
        }
        
        return .none
    }
    
    func finish() {
        
        if let coordinatorDelegate = coordinatorDelegate {
            coordinatorDelegate.dismiss(postDetailCoordinator: coordinatorDelegate)
        }        
    }
    
}
