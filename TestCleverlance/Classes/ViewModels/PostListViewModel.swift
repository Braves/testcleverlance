import Foundation

protocol PostListViewModelViewDelegate: class {
    
    func updateContent(viewModel: ViewModelProtocol)
}

protocol PostListViewModelDelegate: class {
    
    func selectPostAction(_ viewModel: ViewModelProtocol)
}

class PostListViewModel: BaseViewModel {
    
    weak var viewDelegate: PostListViewModelViewDelegate?
    weak var coordinatorDelegate: PostListViewModelDelegate?
    var selectedModel: ModelProtocol?
    var currentPage = 0
    
    var models: [ModelProtocol]? {
        
        didSet {
            
            if let viewDelegate = self.viewDelegate {
                
                viewDelegate.updateContent(viewModel: self)
                self.performAdditionalUpdates()
            }
        }
    }
    
    override func setup() {
        
        downloadListOfPosts()
    }
    
    func downloadListOfPosts() {
        
        currentPage += 1
        
        if let networkingService = networkingService,
            let mappingService = mappingService {
            
            networkingService.getList(forPage: String(currentPage), withBlock: { [unowned self, unowned mappingService] response in
                
                if let response = response {
                    
                    if let deserialized = mappingService.deserializeListOfPosts(response) {
                        
                        if let _ = self.models {
                            
                            var reversedResult:[ModelProtocol] = deserialized.reversed()
                            reversedResult.append(contentsOf: self.models!)
                            
                            self.models = reversedResult
                        } else {
                            
                            self.models = deserialized.reversed()
                        }
                    }
                }
            })
        }
    }
    
    func performAdditionalUpdates() {
        
        if let models = models,
            let networkingService = networkingService,
            let mappingService = mappingService,
            let viewDelegate = viewDelegate {
            
            models.forEach{ [unowned self, unowned networkingService, unowned viewDelegate] model in
                
                if let Post = model as? Post {
                
                    networkingService.getImages(for: Post.dao.imageURL(),
                                                withBlock: { response in

                            if let response = response {
                                
                            Post.dao.picture(with: mappingService.deserializePostImage(response))
                            viewDelegate.updateContent(viewModel: self)
                        }
                    })
                }
            }            
        }
    }
    
    func count() -> Int {
        
        if let models = models {
            
            return models.count
        }
        
        return 0
    }
    
    func title(at index: Int) -> String? {
    
        if let models = models,
            let model = models[index] as? Post {
            
            return  model.dao.title()
        }
        
        return .none
    }
    
    func selectEntry(at index: Int) {
        
        if let coordinatorDelegate = coordinatorDelegate,
            let models = models,
            index > -1,
            models.count > index {
            
            selectedModel = models[index]
            coordinatorDelegate.selectPostAction(self)
        }
    }
    
    func image(at index: Int) -> Data? {
        
        if let models = models,
            let model = models[index] as? Post {
            
            return  model.dao.picture()
        }
        
        return .none
    }
}
