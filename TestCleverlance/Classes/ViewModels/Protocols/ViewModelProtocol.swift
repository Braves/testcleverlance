import Foundation

protocol ViewModelProtocol {
    
    var networkingService: NetworkingProtocol? { get set }
    var mappingService: MappingServiceProtocol? { get set }
    var parentViewModel: ViewModelProtocol? { get set }

    init(withParentViewModel viewModel: ViewModelProtocol?)    
    func setup()
}
